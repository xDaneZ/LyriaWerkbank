package me.wolf2323.lyriawerkbank.calculator;

import java.util.HashMap;


public class Recipe
{
	final String					name;
	final int						count;
	final HashMap<String, Integer>	ingredients;

	public Recipe(String name, int count)
	{
		this.name = name;
		this.count = count;
		ingredients = new HashMap<>();
	}

	public void addIngredient(String recipe, int count)
	{
		if(ingredients.containsKey(recipe))
		{
			count += ingredients.get(recipe);
		}
		ingredients.put(recipe, count);
	}

	public HashMap<String, Integer> getIngredients()
	{
		return ingredients;
	}

	public int getCount()
	{
		return count;
	}

	public String getName()
	{
		return name;
	}

	@Override
	public String toString()
	{
		return ingredients.toString();
	}
}
