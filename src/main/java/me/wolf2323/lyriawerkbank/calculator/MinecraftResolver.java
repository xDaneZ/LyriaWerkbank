package me.wolf2323.lyriawerkbank.calculator;

import java.util.ArrayList;


public enum MinecraftResolver
{
	ACACIA_LOG("minecraft:acacia_log", 1, null, 0, "Akazienstamm"),
	ACACIA_PLANKS("minecraft:acacia_planks", 4, "minecraft:acacia_log", 1, "Akazienholzbretter"),
	ACACIA_WOOD("minecraft:acacia_wood", 3, "minecraft:acacia_log", 4, "Akazienholz"),
	APPLE("minecraft:apple", 1, null, 0, "Apfel"),
	BEEF("minecraft:beef", 1, null, 0, "Rohes Rindfleisch"),
	BEETROOT("minecraft:beetroot", 1, null, 0, "Rote Bete"),
	BIRCH_LOG("minecraft:birch_log", 1, null, 0, "Birkenstamm"),
	BIRCH_PLANKS("minecraft:birch_planks", 4, "minecraft:birch_log", 1, "Birkenholzbretter"),
	BIRCH_SLAB("minecraft:birch_slab", 6, "minecraft:birch_planks", 3, "Birkenholzstufe"),
	BIRCH_WOOD("minecraft:birch_wood", 3, "minecraft:birch_log", 4, "Birkenholz"),
	BLACK_WOOL("minecraft:black_wool", 1, null, 0, "Schwarze Wolle"),
	BLAZE_ROD("minecraft:blaze_rod", 1, null, 0, "Lohenrute"),
	BONE("minecraft:bone", 1, null, 0, "Knochen"),
	BONE_MEAL("minecraft:bone_meal", 3, "minecraft:bone", 1, "Knochenmehl"),
	BREAD("minecraft:bread", 1, "minecraft:wheat", 3, "Brot"),
	BROWN_MUSHROOM("minecraft:brown_mushroom", 1, null, 0, "Brauner Pilz"),
	CACTUS("minecraft:cactus", 1, null, 0, "Kaktus"),
	CARROT("minecraft:carrot", 1, null, 0, "Karotte"),
	CARVED_PUMPKIN("minecraft:carved_pumpkin", 1, null, 0, "Geschnitzter Kürbis"),
	CHICKEN("minecraft:chicken", 1, null, 0, "Rohes Hünchen"),
	COAL("minecraft:coal", 9, "minecraft:coal_block", 1, "Kohle"),
	COAL_BLOCK("minecraft:coal_block", 1, null, 0, "Kohleblock"),
	COBBLESTONE("minecraft:cobblestone", 1, null, 0, "Bruchstein"),
	COCOA_BEANS("minecraft:cocoa_beans", 1, null, 0, "Kakaobohnen"),
	COD("minecraft:cod", 1, null, 0, "Roher Kabeljau"),
	COOKED_BEEF("minecraft:cooked_beef", 1, null, 0, "Steak"),
	COOKIE("minecraft:cookie", 1, null, 0, "Keks"),
	DARK_OAK_LOG("minecraft:dark_oak_log", 1, null, 0, "Schwarzeichenstamm"),
	DARK_OAK_PLANKS("minecraft:dark_oak_planks", 4, "minecraft:dark_oak_log", 1, "Schwarzeichenholzbretter"),
	DARK_OAK_WOOD("minecraft:dark_oak_wood", 3, "minecraft:dark_oak_log", 4, "Schwarzeichenholz"),
	DIAMOND("minecraft:diamond", 9, "minecraft:diamond_block", 1, "Diamant"),
	DIAMOND_BLOCK("minecraft:diamond_block", 1, null, 0, "Diamantblock"),
	DIRT("minecraft:dirt", 1, null, 0, "Erde"),
	EGG("minecraft:egg", 1, null, 0, "Ei"),
	EMERALD("minecraft:emerald", 9, "minecraft:emerald_block", 1, "Smaragd"),
	EMERALD_BLOCK("minecraft:emerald_block", 1, null, 0, "Smaragdblock"),
	ENDER_PEARL("minecraft:ender_pearl", 1, null, 0, "Enderperle"),
	GHAST_TEAR("minecraft:ghast_tear", 1, null, 0, "Ghastträne"),
	GLOWSTONE_DUST("minecraft:glowstone_dust", 1, null, 0, "Glowstonestaub"),
	GOLD_BLOCK("minecraft:gold_block", 1, null, 0, "Goldblock"),
	GOLD_INGOT("minecraft:gold_ingot", 9, "minecraft:gold_block", 1, "Goldbarren"),
	GOLD_NUGGET("minecraft:gold_nugget", 9, "minecraft:gold_ingot", 1, "Goldklumpen"),
	GOLD_ORE("minecraft:gold_ore", 1, null, 0, "Golderz"),
	GRAVEL("minecraft:gravel", 1, null, 0, "Kies"),
	GUNPOWDER("minecraft:gunpowder", 1, null, 0, "Schwarzpulver"),
	HAY_BLOCK("minecraft:hay_block", 1, null, 0, "Strohballen"),
	INK_SAC("minecraft:ink_sac", 1, null, 0, "Tintenbeutel"),
	IRON_BLOCK("minecraft:iron_block", 1, null, 0, "Eisenblock"),
	IRON_INGOT("minecraft:iron_ingot", 9, "minecraft:iron_block", 1, "Eisenbarren"),
	IRON_NUGGET("minecraft:iron_nugget", 9, "minecraft:iron_ingot", 1, "Eisenklumpen"),
	IRON_ORE("minecraft:iron_ore", 1, null, 0, "Eisenerz"),
	JUNGLE_LOG("minecraft:jungle_log", 1, null, 0, "Tropenbaumstamm"),
	JUNGLE_PLANKS("minecraft:jungle_planks", 4, "minecraft:jungle_log", 1, "Tropenholzbretter"),
	JUNGLE_SLAB("minecraft:jungle_slab", 6, "minecraft:jungle_planks", 3, "Tropenholzstufe"),
	JUNGLE_WOOD("minecraft:jungle_wood", 3, "minecraft:jungle_log", 4, "Tropenholz"),
	LAPIS_BLOCK("minecraft:lapis_block", 1, null, 0, "Lapislazuliblock"),
	LAPIS_LAZULI("minecraft:lapis_lazuli", 9, "minecraft:lapis_block", 1, "Lapislazuli"),
	LEAD("minecraft:lead", 1, null, 0, "Leine"),
	LEATHER("minecraft:leather", 1, null, 0, "Leder"),
	LIGHT_BLUE_WOOL("minecraft:light_blue_wool", 1, null, 0, "Hellblaue Wolle"),
	LIME_WOOL("minecraft:lime_wool", 1, null, 0, "Hellgrüne Wolle"),
	MAGMA_CREAM("minecraft:magma_cream", 1, null, 0, "Magmacreme"),
	MELON_SLICE("minecraft:melon_slice", 1, null, 0, "Melonenscheibe"),
	NETHER_WART("minecraft:nether_wart", 1, null, 0, "Netherwarze"),
	OAK_LEAVES("minecraft:oak_leaves", 1, null, 0, "Eichenlaub"),
	OAK_LOG("minecraft:oak_log", 1, null, 0, "Eichenstamm"),
	OAK_PLANKS("minecraft:oak_planks", 4, "minecraft:oak_log", 1, "Eichenholzbretter"),
	OAK_WOOD("minecraft:oak_wood", 3, "minecraft:oak_log", 4, "Eichenholz"),
	OBSIDIAN("minecraft:obsidian", 1, null, 0, "Obsidian"),
	ORANGE_TULIP("minecraft:orange_tulip", 1, null, 0, "Orange Tulpe"),
	PAPER("minecraft:paper", 3, "minecraft:sugar_cane", 3, "Papier"),
	POISONOUS_POTATO("minecraft:poisonous_potato", 1, null, 0, "Giftige Kartoffel"),
	PORKCHOP("minecraft:porkchop", 1, null, 0, "Rohes Schweinefleisch"),
	POTATO("minecraft:potato", 1, null, 0, "Kartoffel"),
	POTION("minecraft:potion", 1, null, 0, "Wasserflasche"),
	PRISMARINE("minecraft:prismarine", 1, null, 0, "Prismarin"),
	PRISMARINE_SHARD("minecraft:prismarine_shard", 1, null, 0, "Prismarinscherbe"),
	PUFFERFISH("minecraft:pufferfish", 1, null, 0, "Kugelfisch"),
	PUMPKIN("minecraft:pumpkin", 1, null, 0, "Kürbis"),
	PUMPKIN_PIE("minecraft:pumpkin_pie", 1, null, 0, "Kürbiskuchen"),
	PUMPKIN_SEEDS("minecraft:pumpkin_seeds", 3, "minecraft:pumpkin", 1, "Kürbiskerne"),
	PURPLE_DYE("minecraft:purple_dye", 1, null, 0, "Violetter Farbstoff"),
	QUARTZ("minecraft:quartz", 1, null, 0, "Netherquarz"),
	QUARTZ_BLOCK("minecraft:quartz_block", 1, "minecraft:quartz", 4, "Quarzblock"),
	QUARTZ_SLAB("minecraft:quartz_slab", 6, "minecraft:quartz_block", 3, "Quarzstufe"),
	RED_MUSHROOM("minecraft:red_mushroom", 1, null, 0, "Roter Pilz"),
	RED_SANDSTONE_SLAB("minecraft:red_sandstone_slab", 6, "minecraft:red_sandstone", 3, "Rote Sandsteinstufe"),
	RED_SANDSTONE("minecraft:red_sandstone", 1, "minecraft:red_sand", 4, "Roter Sandstein"),
	RED_SAND("minecraft:red_sand", 1, null, 0, "Roter Sand"),
	RED_WOOL("minecraft:red_wool", 1, null, 0, "Rote Wolle"),
	REDSTONE("minecraft:redstone", 9, "minecraft:redstone_block", 1, "Redstone"),
	REDSTONE_BLOCK("minecraft:redstone_block", 1, null, 0, "Redstone-Block"),
	ROTTEN_FLESH("minecraft:rotten_flesh", 1, null, 0, "Verrottetes Fleisch"),
	SALMON("minecraft:salmon", 1, null, 0, "Roher Lachs"),
	SAND("minecraft:sand", 1, null, 0, "Sand"),
	SANDSTONE("minecraft:sandstone", 1, "minecraft:sand", 4, "Sandstein"),
	SANDSTONE_SLAB("minecraft:sandstone_slab", 6, "minecraft:sandstone", 3, "Sandsteinstufe"),
	SPIDER_EYE("minecraft:spider_eye", 1, null, 0, "Spinnenauge"),
	SPRUCE_LOG("minecraft:spruce_log", 1, null, 0, "Fichtenstamm"),
	SPRUCE_PLANKS("minecraft:spruce_planks", 4, "minecraft:spruce_log", 1, "Fichtenholzbretter"),
	SPRUCE_SLAB("minecraft:spruce_slab", 6, "minecraft:spruce_planks", 3, "Fichtenholzstufe"),
	SPRUCE_WOOD("minecraft:spruce_wood", 3, "minecraft:spruce_log", 4, "Fichtenholz"),
	STICK("minecraft:stick", 1, null, 0, "Stock"),
	STONE("minecraft:stone", 1, null, 0, "Stein"),
	STONE_BRICKS("minecraft:stone_bricks", 4, "minecraft:stone", 4, "Steinziegel"),
	STONE_SLAB("minecraft:stone_slab", 6, "minecraft:stone", 3, "Steinstufe"),
	STRING("minecraft:string", 1, null, 0, "Faden"),
	SUGAR("minecraft:sugar", 1, "minecraft:sugar_cane", 1, "Zucker"),
	SUGAR_CANE("minecraft:sugar_cane", 1, null, 0, "Zuckerrohr"),
	WHEAT("minecraft:wheat", 9, "minecraft:hay_block", 1, "Weizen"),
	WHEAT_SEEDS("minecraft:wheat_seeds", 1, null, 0, "Weizenkörner"),
	WHITE_WOOL("minecraft:white_wool", 1, null, 0, "Weiße Wolle"),
	WOODEN_SHOVEL("minecraft:wooden_shovel", 1, null, 0, "Holzschaufel");

	private String	key;
	private Integer	out;
	private String	resource;
	private Integer	in;
	private String	toReadable;

	private MinecraftResolver(String key, Integer out, String resource, Integer in, String toReadable)
	{
		this.key = key;
		this.out = out;
		this.resource = resource;
		this.in = in;
		this.toReadable = toReadable;
	}

	public static MinecraftResolver getByKey(String key)
	{
		for(MinecraftResolver e : MinecraftResolver.values())
		{
			if(e.getKey().equals(key))
			{
				return e;
			}
		}
		return null;
	}

	public static ArrayList<MinecraftResolver> getByTarget(String key)
	{
		ArrayList<MinecraftResolver> targets = new ArrayList<>();
		for(MinecraftResolver e : MinecraftResolver.values())
		{
			if(e.getTarget() != null && e.getTarget().equals(key))
			{
				targets.add(e);
			}
		}
		return targets;
	}

	public static MinecraftResolver getByReadable(String display)
	{
		for(MinecraftResolver e : MinecraftResolver.values())
		{
			if(e.getToReadable().equals(display))
			{
				return e;
			}
		}
		return null;
	}

	public String getKey()
	{
		return key;
	}

	public Integer getKeyAmount()
	{
		return out;
	}

	public String getTarget()
	{
		return resource;
	}

	public Integer getTargetAmount()
	{
		return in;
	}

	public String getToReadable()
	{
		return toReadable;
	}
}
