package me.wolf2323.lyriawerkbank.calculator;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import me.wolf2323.lyriawerkbank.calculator.excel.ExcelItemWriter;
import me.wolf2323.lyriawerkbank.util.SingletonArrayList;
import me.wolf2323.lyriawerkbank.util.Utils;


public class Calculator
{
	private static final Gson				GSON	= new GsonBuilder().setLenient().create();
	private static HashMap<String, Recipe>	recipes;

	public static void loadRecipes() throws IOException, URISyntaxException
	{
		String path = "data/lyriawerkbank/recipes";
		URI jarURI = Calculator.class.getProtectionDomain().getCodeSource().getLocation().toURI();
		Path jarPath = Paths.get(jarURI);
		if(Files.isRegularFile(jarPath))
		{
			try(final FileSystem fs = Utils.getZipFileSystem(jarURI))
			{
				recipes = loadFiles(fs.getPath(path));
			}
		}
		else
		{
			recipes = loadFiles(jarPath.resolve(path));
		}

		for(Recipe r : recipes.values())
		{
			for(String s : r.getIngredients().keySet())
			{
				if(s.startsWith("minecraft:"))
				{
					if(MinecraftResolver.getByKey(s) == null)
					{
						System.out.println("Fehlendes MC Item: " + s);
					}
				}
			}
		}

	}

	private static HashMap<String, Recipe> loadFiles(Path file) throws IOException
	{
		HashMap<String, Recipe> recipes = new HashMap<>();

		try
		{
			Files.walk(file).filter(path ->
			{
				if(Files.isRegularFile(path))
				{
					final String name = path.getFileName().toString();
					if(!name.equals("_constants.json"))
					{
						return true;
					}
				}
				return false;
			}).forEach(path ->
			{
				try
				{
					InputStream stream = Files.newInputStream(path, StandardOpenOption.READ);

					String jsonTxt = IOUtils.toString(stream, "UTF-8");
					JsonObject json = GSON.fromJson(jsonTxt, JsonObject.class);

					Entry<String, Recipe> recipe = parseRecipe(json);
					recipes.put(recipe.getKey(), recipe.getValue());
				}
				catch(IOException e)
				{
					throw new RuntimeException(e);
				}
			});
		}
		catch(RuntimeException e)
		{
			throw new IOException(e);
		}
		return recipes;
	}

	private static Entry<String, Recipe> parseRecipe(JsonObject recipe)
	{
		String name = recipe.get("group").getAsString();
		name = name.substring("lyriawerkbank:".length());
		String nameReadable = recipe.get("name").getAsString();

		JsonObject result = recipe.get("result").getAsJsonObject();
		int resultAmount = result.has("count") ? result.get("count").getAsInt() : 1;

		HashMap<Character, Integer> keys = new HashMap<>();
		for(JsonElement e : recipe.get("pattern").getAsJsonArray())
		{
			for(char c : e.getAsString().toCharArray())
			{
				int count = 1;
				if(keys.containsKey(c))
				{
					count += keys.get(c);
				}
				keys.put(c, count);
			}
		}

		Recipe recipeResult = new Recipe(nameReadable, resultAmount);
		for(Entry<String, JsonElement> e : recipe.get("key").getAsJsonObject().entrySet())
		{
			JsonObject obj = e.getValue().getAsJsonObject();
			String item = obj.has("item") ? obj.get("item").getAsString() : obj.get("constant").getAsString().substring("lyriawerkbank:".length());
			int count = obj.has("count") ? obj.get("count").getAsInt() : 1;
			int amount = keys.get(e.getKey().toCharArray()[0]);
			recipeResult.addIngredient(item, count * amount);
		}

		return new AbstractMap.SimpleEntry<String, Recipe>(name, recipeResult);
	}

	public static String calculate(String name) throws IOException, NoSuchFieldException
	{
		String nameOrig = parseInputName(name);

		ArrayList<String> ingredients = new SingletonArrayList<>();
		ArrayList<String> lyriaRecipes = new SingletonArrayList<>();
		getRecipeTree(ingredients, lyriaRecipes, nameOrig);

		Collections.sort(ingredients);
		ArrayList<ArrayList<String>> lyriaRecipesStack = getSortedRecipes(ingredients, lyriaRecipes);

		ExcelItemWriter.write(nameOrig, ingredients, lyriaRecipesStack, recipes);

		return recipes.get(nameOrig).getName();
	}

	private static String parseInputName(String name) throws NoSuchFieldException
	{
		String nameOrig = null;
		for(Entry<String, Recipe> e : recipes.entrySet())
		{
			if(e.getValue().getName().equalsIgnoreCase(name))
			{
				nameOrig = e.getKey();
				break;
			}
		}
		if(nameOrig == null)
		{
			throw new NoSuchFieldException("Item nicht gefunden!");
		}
		return nameOrig;
	}

	private static void getRecipeTree(ArrayList<String> ingredients, ArrayList<String> lyriaIngredients, String subGoal)
	{
		Recipe recipe = recipes.get(subGoal);
		if(recipe == null)
		{
			ingredients.add(subGoal);
		}
		else
		{
			lyriaIngredients.add(subGoal);
			for(Entry<String, Integer> i : recipe.getIngredients().entrySet())
			{
				getRecipeTree(ingredients, lyriaIngredients, i.getKey());
			}
		}

	}

	private static ArrayList<ArrayList<String>> getSortedRecipes(ArrayList<String> ingredients, ArrayList<String> lyriaRecipes)
	{
		ArrayList<ArrayList<String>> result = new ArrayList<>();
		ArrayList<String> lyriaRecipesCopy = new ArrayList<>(lyriaRecipes);

		int index = 0;
		boolean oneAccepted = false;
		ArrayList<String> iterrationArray = new ArrayList<>();
		while(!lyriaRecipesCopy.isEmpty())
		{
			String name = lyriaRecipesCopy.get(index);
			Recipe recipe = recipes.get(name);
			boolean contains = true;
			recipes : for(String ing : recipe.getIngredients().keySet())
			{
				for(ArrayList<String> array : result)
				{
					if((array.contains(ing)))
					{
						continue recipes;
					}
				}
				if(!ingredients.contains(ing))
				{
					contains = false;
					break;
				}
			}

			if(contains)
			{
				iterrationArray.add(name);
				lyriaRecipesCopy.remove(index);
				oneAccepted = true;
			}
			else
			{
				index++;
			}
			if(index >= lyriaRecipesCopy.size())
			{
				index = 0;
				result.add(iterrationArray);
				iterrationArray = new ArrayList<>();
				if(!oneAccepted)
				{
					System.out.println(result);
					System.out.println(lyriaRecipesCopy);
					throw new ArithmeticException("Keine Zutat konnte aufgelöst werden");
				}
				oneAccepted = false;
			}
		}

		ArrayList<ArrayList<String>> resultReordert = new ArrayList<>();
		for(int i = result.size() - 1; i >= 0; i--)
		{
			resultReordert.add(result.get(i));
		}

		return resultReordert;
	}

	public static HashMap<String, Recipe> getRecipes()
	{
		return recipes;
	}
}
