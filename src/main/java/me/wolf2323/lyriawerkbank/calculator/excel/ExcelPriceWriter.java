package me.wolf2323.lyriawerkbank.calculator.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import me.wolf2323.lyriawerkbank.calculator.MinecraftResolver;
import me.wolf2323.lyriawerkbank.calculator.excel.ExcelWriter.ExcelSheet;


public class ExcelPriceWriter
{
	protected static String priceTable = "Preise";

	public static void write() throws IOException
	{
		XSSFWorkbook workbook = ExcelWriter.loadWorkbook();

		if(workbook.getSheet(priceTable) != null)
		{
			throw new IOException(priceTable + " wurde bereits eingerichtet!");
		}

		Sheet workbookSheet = workbook.createSheet(priceTable);
		workbookSheet.setColumnWidth(0, 7000);
		ExcelSheet sheet = new ExcelSheet(workbookSheet);

		writeSheet(sheet);

		ExcelWriter.saveWorkbook(workbook);
	}

	private static void writeSheet(ExcelSheet sheet)
	{
		int offset = 0;

		offset = writeHeaderBiggestItems(sheet, offset);
		ArrayList<String> itemsBig = getBiggestItemTypes();
		Collections.sort(itemsBig);
		offset = writeBiggestItemsPrices(sheet, offset, itemsBig);

		offset += 2;

		offset = writeHeaderAllItems(sheet, offset);
		ArrayList<String> itemsAll = getAllItemTypes();
		itemsAll.removeAll(itemsBig);
		Collections.sort(itemsAll);
		offset = writeAllItemsPrices(sheet, offset, itemsAll);
	}

	private static int writeHeaderBiggestItems(ExcelSheet sheet, int offset)
	{
		Cell cell = sheet.getCell(offset++, 0);
		cell.setCellValue("Preisliste");
		cell.setCellStyle(ExcelWriter.headerStyle);

		offset++;
		cell = sheet.getCell(offset, 0);
		cell.setCellValue("Name");
		cell.setCellStyle(ExcelWriter.headerStyleSub);

		cell = sheet.getCell(offset, 1);
		cell.setCellValue("Preis");
		cell.setCellStyle(ExcelWriter.headerStyleSub);

		offset++;
		return offset;
	}

	private static int writeBiggestItemsPrices(ExcelSheet sheet, int offset, ArrayList<String> itemsBig)
	{
		for(int i = 0; i < itemsBig.size(); i++)
		{
			int row = offset++;
			Cell cell = sheet.getCell(row, 0);
			cell.setCellValue(itemsBig.get(i));
			cell = sheet.getCell(row, 1);
			cell.setCellValue("Preis eintragen");
		}
		return offset;
	}

	private static int writeHeaderAllItems(ExcelSheet sheet, int offset)
	{
		Cell cell = sheet.getCell(offset++, 0);
		cell.setCellValue("Einzelpreise");
		cell.setCellStyle(ExcelWriter.headerStyle);

		offset++;
		cell = sheet.getCell(offset, 0);
		cell.setCellValue("Name");
		cell.setCellStyle(ExcelWriter.headerStyleSub);

		cell = sheet.getCell(offset, 1);
		cell.setCellValue("Preis");
		cell.setCellStyle(ExcelWriter.headerStyleSub);

		offset++;
		return offset;
	}

	private static int writeAllItemsPrices(ExcelSheet sheet, int offset, ArrayList<String> itemsAll)
	{
		for(int i = 0; i < itemsAll.size(); i++)
		{
			int row = offset++;
			Cell cell = sheet.getCell(row, 0);
			cell.setCellValue(itemsAll.get(i));
			cell = sheet.getCell(row, 1);
			cell.setCellFormula(getItemPrice(itemsAll.get(i)));
		}
		return offset;
	}

	private static String getItemPrice(String name)
	{
		StringBuilder builder = new StringBuilder();

		MinecraftResolver resolver = MinecraftResolver.getByReadable(name);
		String resolvedName = MinecraftResolver.getByKey(resolver.getTarget()).getToReadable();

		builder.append("INDEX(B:B,MATCH(\"").append(resolvedName).append("\",A:A,0))/").append(resolver.getKeyAmount()).append("*").append(resolver.getTargetAmount());

		return builder.toString();
	}

	private static ArrayList<String> getBiggestItemTypes()
	{
		ArrayList<String> names = new ArrayList<>();
		for(MinecraftResolver res : MinecraftResolver.values())
		{
			if(res.getTarget() == null)
			{
				names.add(res.getToReadable());
			}
		}
		return names;
	}

	private static ArrayList<String> getAllItemTypes()
	{
		ArrayList<String> names = new ArrayList<>();
		for(MinecraftResolver res : MinecraftResolver.values())
		{
			names.add(res.getToReadable());
		}
		return names;
	}
}
