package me.wolf2323.lyriawerkbank.calculator.excel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import me.wolf2323.lyriawerkbank.calculator.Calculator;
import me.wolf2323.lyriawerkbank.calculator.MinecraftResolver;
import me.wolf2323.lyriawerkbank.calculator.Recipe;
import me.wolf2323.lyriawerkbank.calculator.excel.ExcelWriter.ExcelSheet;
import me.wolf2323.lyriawerkbank.util.SingletonArrayList;


public class ExcelItemWriter
{
	private static int space = 8 + 1;

	public static void write(String name, ArrayList<String> ingredients, ArrayList<ArrayList<String>> lyriaRecipesStack, HashMap<String, Recipe> recipes)
			throws IOException
	{
		XSSFWorkbook workbook = ExcelWriter.loadWorkbook();

		if(workbook.getSheet(name) != null)
		{
			throw new IOException(name + " wurde bereits berechnet!");
		}
		Sheet workbookSheet = workbook.createSheet(name);
		setColumnWidths(workbookSheet, lyriaRecipesStack);
		ExcelSheet sheet = new ExcelSheet(workbookSheet);

		writeSheet(sheet, ingredients, lyriaRecipesStack, recipes, name);

		ExcelWriter.saveWorkbook(workbook);
	}

	private static void setColumnWidths(Sheet sheet, ArrayList<ArrayList<String>> lyriaRecipesStack)
	{
		int size = 2;
		for(ArrayList<String> a : lyriaRecipesStack)
		{
			if(size < a.size())
			{
				size = a.size();
			}
		}

		for(int i = 0; i < size; i++)
		{
			int cellOffset = i * (space);
			sheet.setColumnWidth(cellOffset, 7000);
			sheet.setColumnWidth(cellOffset + 1, 3500);

			for(int fix = 2; fix < 5; fix++)
			{
				sheet.setColumnWidth(cellOffset + fix, 1000);
			}

			sheet.setColumnWidth(cellOffset + 5, 3500);

			for(int fix = 6; fix < 9; fix++)
			{
				sheet.setColumnWidth(cellOffset + fix, 3500);
			}
		}
	}

	private static void writeSheet(ExcelSheet sheet, ArrayList<String> ingredients, ArrayList<ArrayList<String>> lyriaRecipesStack, HashMap<String, Recipe> recipes,
			String recipeName)
	{
		int offset = 0;
		offset = generateHeader(sheet, offset);

		HashMap<String, Pair<Cell, Cell>> cacheOverviewLyriaRecipesAmountPrice = new HashMap<>();
		HashMap<String, Cell> cacheOverviewIngredientsAmount = new HashMap<>();
		offset = generateOverview(sheet, offset, cacheOverviewLyriaRecipesAmountPrice, cacheOverviewIngredientsAmount, ingredients, lyriaRecipesStack);

		offset += 2;
		offset = generateCraftHeader(sheet, offset);

		HashMap<String, ArrayList<CellReference>> cacheIngredientsAmount = getDefaultCacheIngredientAmount(recipeName);
		offset = generateIngredients(sheet, offset, cacheOverviewLyriaRecipesAmountPrice, cacheIngredientsAmount, ingredients, lyriaRecipesStack, recipes);

		generateIngredientsSum(cacheOverviewIngredientsAmount, cacheIngredientsAmount);
	}

	private static int generateHeader(ExcelSheet sheet, int offset)
	{
		Cell cell = sheet.getCell(offset, 0);
		cell.setCellValue("Anzahl:");
		cell.setCellStyle(ExcelWriter.headerStyle);
		cell = sheet.getCell(offset, 1);
		cell.setCellValue(1);
		cell.setCellStyle(ExcelWriter.headerStyle);
		cell = sheet.getCell(offset, 5);
		cell.setCellValue("Legende");
		cell.setCellStyle(ExcelWriter.headerStyle);
		cell = sheet.getCell(offset, 6);
		cell.setCellValue("I = Items");
		cell = sheet.getCell(offset, 7);
		cell.setCellValue("S = Stacks");
		cell = sheet.getCell(offset, 8);
		cell.setCellValue("DK = Doppelkisten");

		offset += 2;
		cell = sheet.getCell(offset, 0);
		cell.setCellValue("Farming");
		cell.setCellStyle(ExcelWriter.headerStyle);
		cell = sheet.getCell(offset, space);
		cell.setCellValue("Crafting");
		cell.setCellStyle(ExcelWriter.headerStyle);

		offset++;
		cell = sheet.getCell(offset, 0);
		cell.setCellValue("Farme alle diese Ressourcen um alles weitere Craften zu können");
		cell = sheet.getCell(offset, space);
		cell.setCellValue("Aus 'Farming' werden nun folgende dinge hergestellt");

		offset++;
		cell = sheet.getCell(offset, 0);
		cell.setCellValue("Hier wird alles in möglichst großen Einheiten zusammengefasst angegeben");
		cell = sheet.getCell(offset, space);
		cell.setCellValue("Die Reihenfolge ist von unten nach oben, Bestände sind nicht mehr enthalten");

		offset += 7;

		for(int i = 0; i < 2; i++)
		{
			int cellOffset = i * (space);

			cell = sheet.getCell(offset, cellOffset);
			cell.setCellValue("Name");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 1);
			cell.setCellValue("Items gesamt");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 2);
			cell.setCellValue("I");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 3);
			cell.setCellValue("S");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 4);
			cell.setCellValue("DK");
			cell.setCellStyle(ExcelWriter.headerStyleSub);

			cell = sheet.getCell(offset, cellOffset + 5);
			cell.setCellValue("Einzel Preis");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 6);
			cell.setCellValue("Gesamt Preis");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
		}

		offset++;
		return offset;
	}

	private static int generateOverview(ExcelSheet sheet, int offset, HashMap<String, Pair<Cell, Cell>> cacheOverviewLyriaRecipesAmountPrice,
			HashMap<String, Cell> cacheOverviewIngredientsAmount, ArrayList<String> ingredients, ArrayList<ArrayList<String>> lyriaRecipesStack)
	{
		ArrayList<String> farming = getFarmingOverviewList(ingredients);
		ArrayList<String> crafting = getCraftingOverviewList(lyriaRecipesStack);

		int size = farming.size() > crafting.size() ? farming.size() : crafting.size();

		for(int index = 0; index < size; index++)
		{
			for(int i = 0; i < 2; i++)
			{
				boolean isZero = i == 0;
				int cellOffset = i * (space);

				String name;
				if(isZero && farming.size() > index)
				{
					name = farming.get(index);
				}
				else if(!isZero && crafting.size() > index)
				{
					name = crafting.get(index);
				}
				else
				{
					continue;
				}

				Cell cell = sheet.getCell(offset, cellOffset);
				cell.setCellValue(toReadable(name));
				CellReference cellName = new CellReference(cell);

				cell = sheet.getCell(offset, cellOffset + 1);
				if(isZero)
				{
					cacheOverviewIngredientsAmount.put(name, cell);
				}
				else
				{
					cacheOverviewLyriaRecipesAmountPrice.put(name, Pair.of(cell, null));
				}
				CellReference cellItemAmount = new CellReference(cell);

				cell = sheet.getCell(offset, cellOffset + 2);
				cell.setCellFormula(getItemAmount(cellItemAmount));

				cell = sheet.getCell(offset, cellOffset + 3);
				cell.setCellFormula(getStackAmount(cellItemAmount));

				cell = sheet.getCell(offset, cellOffset + 4);
				cell.setCellFormula(getDKAmount(cellItemAmount));

				if(isZero)
				{
					cell = sheet.getCell(offset, cellOffset + 5);
					cell.setCellFormula(getPriceFromPriceSheet(cellName));
				}
				else
				{
					cell = sheet.getCell(offset, cellOffset + 5);
					cacheOverviewLyriaRecipesAmountPrice.put(name, Pair.of(cacheOverviewLyriaRecipesAmountPrice.get(name).getLeft(), cell));
				}
				CellReference price = new CellReference(cell);
				cell = sheet.getCell(offset, cellOffset + 6);
				cell.setCellFormula(getAbsolutPrice(cellItemAmount, price));
			}
			offset++;
		}

		return offset;
	}

	private static ArrayList<String> getFarmingOverviewList(ArrayList<String> ingredients)
	{
		SingletonArrayList<String> result = new SingletonArrayList<>();

		for(String s : ingredients)
		{
			MinecraftResolver res = getBiggestItemType(s);
			result.add(res == null ? s : res.getToReadable());
		}

		Collections.sort(result);
		return result;
	}

	private static ArrayList<String> getCraftingOverviewList(ArrayList<ArrayList<String>> lyriaRecipesStack)
	{
		ArrayList<String> result = new ArrayList<>();

		for(ArrayList<String> list : lyriaRecipesStack)
		{
			result.addAll(list);
		}
		return result;
	}

	private static MinecraftResolver getBiggestItemType(String mcItem)
	{
		MinecraftResolver res = MinecraftResolver.getByKey(mcItem);
		if(res == null)
		{
			return null;
		}
		if(res.getTarget() == null)
		{
			return res;
		}
		return getBiggestItemType(res.getTarget());
	}

	private static String getPriceFromPriceSheet(CellReference cellName)
	{
		StringBuilder builder = new StringBuilder("INDEX(" + ExcelPriceWriter.priceTable + "!B:B,MATCH(");
		builder.append(cellName.formatAsString()).append("," + ExcelPriceWriter.priceTable + "!A:A,0))");

		return builder.toString();
	}

	private static String getAbsolutPrice(CellReference itemAmount, CellReference price)
	{
		return itemAmount.formatAsString() + "*" + price.formatAsString();
	}

	private static int generateCraftHeader(ExcelSheet sheet, int offset)
	{
		Cell cell = sheet.getCell(offset, 0);
		cell.setCellValue("Crafting details");
		cell.setCellStyle(ExcelWriter.headerStyle);

		offset += 2;
		return offset;
	}

	private static int generateIngredients(ExcelSheet sheet, int offset, HashMap<String, Pair<Cell, Cell>> cacheOverviewLyriaRecipes,
			HashMap<String, ArrayList<CellReference>> cacheIngredientsAmount, ArrayList<String> ingredients, ArrayList<ArrayList<String>> lyriaRecipesStack,
			HashMap<String, Recipe> recipes)
	{
		HashMap<String, HashMap<Pair<String, CellReference>, CellReference>> cacheIngredientsList = new HashMap<>();
		HashMap<String, Pair<CellReference, Cell>> cacheSoloPrice = new HashMap<>();
		for(ArrayList<String> recipesStack : lyriaRecipesStack)
		{
			int size = recipesStack.size();

			offset = generateIngredientsHeader(sheet, offset, size);

			MutableInt biggestRecipe = new MutableInt(2);
			HashMap<String, CellReference> cacheCraftAmount = new HashMap<>();
			offset = generateIngredientsResult(sheet, offset, cacheOverviewLyriaRecipes, cacheIngredientsAmount, cacheCraftAmount, cacheSoloPrice, recipes, recipesStack,
					size, biggestRecipe);
			int tempOffset = offset;

			offset = generateIngredientsInput(sheet, offset, cacheIngredientsAmount, cacheCraftAmount, cacheIngredientsList, recipes, recipesStack, size, biggestRecipe
					.intValue());

			offset = offset > tempOffset ? offset + 2 : tempOffset + 4;
		}

		generatePrice(cacheSoloPrice, cacheIngredientsList);

		return offset;
	}

	private static void generatePrice(HashMap<String, Pair<CellReference, Cell>> cacheSoloPrice,
			HashMap<String, HashMap<Pair<String, CellReference>, CellReference>> cacheIngredientsList)
	{
		for(Entry<String, Pair<CellReference, Cell>> priceCells : cacheSoloPrice.entrySet())
		{
			String name = priceCells.getKey();
			CellReference itemOutputAmount = priceCells.getValue().getLeft();
			Cell price = priceCells.getValue().getRight();
			price.setCellFormula(getPriceForOneItem(cacheSoloPrice, cacheIngredientsList.get(name), itemOutputAmount));
		}
	}

	private static String getPriceForOneItem(HashMap<String, Pair<CellReference, Cell>> cacheSoloPrice,
			HashMap<Pair<String, CellReference>, CellReference> subCacheIngreadients, CellReference itemOutputAmount)
	{
		StringBuilder builder = new StringBuilder();

		builder.append("SUM(");

		for(Entry<Pair<String, CellReference>, CellReference> ing : subCacheIngreadients.entrySet())
		{
			String name = ing.getKey().getKey();
			MinecraftResolver resolver = MinecraftResolver.getByKey(name);
			if(resolver == null)
			{
				CellReference priceRef = new CellReference(cacheSoloPrice.get(name).getRight());
				builder.append(priceRef.formatAsString());
			}
			else
			{
				builder.append(getPriceFromPriceSheet(ing.getKey().getRight()));
			}
			builder.append("*").append(ing.getValue().formatAsString()).append(",");
		}
		builder.deleteCharAt(builder.length() - 1).append(")/").append(itemOutputAmount.formatAsString());

		return builder.toString();

	}

	private static HashMap<String, ArrayList<CellReference>> getDefaultCacheIngredientAmount(String name)
	{
		HashMap<String, ArrayList<CellReference>> cacheIngredientsAmount = new HashMap<>();
		ArrayList<CellReference> cellReferences = new ArrayList<>();
		cellReferences.add(new CellReference(0, 1));
		cacheIngredientsAmount.put(name, cellReferences);
		return cacheIngredientsAmount;
	}

	private static int generateIngredientsHeader(ExcelSheet sheet, int offset, int size)
	{
		for(int i = 0; i < size; i++)
		{
			int cellOffset = i * space;
			Cell cell = sheet.getCell(offset, cellOffset);
			cell.setCellValue("Name");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 1);
			cell.setCellValue("Items gesamt");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 2);
			cell.setCellValue("I");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 3);
			cell.setCellValue("S");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 4);
			cell.setCellValue("DK");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 5);
			cell.setCellValue("In/Out");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset, cellOffset + 6);
			cell.setCellValue("Bestand");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset + 2, cellOffset + 6);
			cell.setCellValue("Zu Craften");
			cell.setCellStyle(ExcelWriter.headerStyleSub);

			cell = sheet.getCell(offset, cellOffset + 7);
			cell.setCellValue("Einzel Preis");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
			cell = sheet.getCell(offset + 2, cellOffset + 7);
			cell.setCellValue("Gesamt Preis");
			cell.setCellStyle(ExcelWriter.headerStyleSub);
		}

		offset++;
		return offset;
	}

	private static int generateIngredientsResult(ExcelSheet sheet, int offset, HashMap<String, Pair<Cell, Cell>> cacheOverviewLyriaRecipes,
			HashMap<String, ArrayList<CellReference>> cacheIngredientsAmount, HashMap<String, CellReference> cacheCraftAmount,
			HashMap<String, Pair<CellReference, Cell>> cacheSoloPrice, HashMap<String, Recipe> recipes, ArrayList<String> recipesStack, int size,
			MutableInt biggestRecipe)
	{
		for(int i = 0; i < size; i++)
		{
			int cellOffset = i * space;
			String name = recipesStack.get(i);
			Recipe recipe = recipes.get(name);

			Cell cell = sheet.getCell(offset, cellOffset);
			cell.setCellValue(name);

			cell = sheet.getCell(offset, cellOffset + 1);
			cell.setCellFormula(getItemSum(cacheIngredientsAmount.get(name)));
			CellReference itemAmount = new CellReference(cell);
			cacheOverviewLyriaRecipes.get(name).getLeft().setCellFormula(itemAmount.formatAsString());

			cell = sheet.getCell(offset, cellOffset + 2);
			cell.setCellFormula(getItemAmount(itemAmount));

			cell = sheet.getCell(offset, cellOffset + 3);
			cell.setCellFormula(getStackAmount(itemAmount));

			cell = sheet.getCell(offset, cellOffset + 4);
			cell.setCellFormula(getDKAmount(itemAmount));

			cell = sheet.getCell(offset, cellOffset + 5);
			cell.setCellValue(recipe.getCount());
			CellReference itemOut = new CellReference(cell);

			cell = sheet.getCell(offset, cellOffset + 6);
			cell.setCellValue(0);
			CellReference have = new CellReference(cell);

			cell = sheet.getCell(offset + 2, cellOffset + 6);
			cell.setCellFormula(getCraftingAmount(itemAmount, have, itemOut));
			CellReference toCraft = new CellReference(cell);
			cacheCraftAmount.put(name, toCraft);

			cell = sheet.getCell(offset, cellOffset + 7);
			cacheOverviewLyriaRecipes.get(name).getRight().setCellFormula((new CellReference(cell)).formatAsString());
			cacheSoloPrice.put(name, Pair.of(itemOut, cell));
			CellReference soloPriceCell = new CellReference(cell);
			cell = sheet.getCell(offset + 2, cellOffset + 7);
			cell.setCellFormula(getAbsolutPrice(itemAmount, soloPriceCell));

			if(recipe.getIngredients().size() > biggestRecipe.getValue())
			{
				biggestRecipe.setValue(recipe.getIngredients().size());
			}
		}
		offset++;
		return offset;
	}

	private static String getItemSum(ArrayList<CellReference> arrayList)
	{
		StringBuilder builder = new StringBuilder("SUM(");

		if(arrayList != null && !arrayList.isEmpty())
		{
			for(CellReference ref : arrayList)
			{
				builder.append(ref.formatAsString()).append(",");
			}
			builder.deleteCharAt(builder.length() - 1);
		}
		else
		{
			builder.append("0");
		}
		return builder.append(")").toString();
	}

	private static String getCraftingAmount(CellReference itemAmount, CellReference have, CellReference itemOut)
	{
		StringBuilder builder = new StringBuilder("ROUNDUP(IF(");
		builder.append(itemAmount.formatAsString()).append("-").append(have.formatAsString());
		builder.append(" < 0, 0, ").append(itemAmount.formatAsString()).append("-").append(have.formatAsString());
		builder.append(")/").append(itemOut.formatAsString()).append(",0)");

		return builder.toString();
	}

	private static int generateIngredientsInput(ExcelSheet sheet, int offset, HashMap<String, ArrayList<CellReference>> cacheIngredientsAmount,
			HashMap<String, CellReference> cacheCraftAmount, HashMap<String, HashMap<Pair<String, CellReference>, CellReference>> cacheIngredientsList,
			HashMap<String, Recipe> recipes, ArrayList<String> recipesStack, int size, int biggestRecipe)
	{
		for(int ingIndex = 0; ingIndex < biggestRecipe; ingIndex++)
		{
			for(int i = 0; i < size; i++)
			{
				int cellOffset = i * space;
				String name = recipesStack.get(i);
				Recipe recipe = recipes.get(name);
				if(ingIndex < recipe.getIngredients().size())
				{
					String ingName = (String) recipe.getIngredients().keySet().toArray()[ingIndex];
					Cell cell = sheet.getCell(offset, cellOffset);
					cell.setCellValue(toReadable(ingName));
					CellReference cellIngName = new CellReference(cell);

					cell = sheet.getCell(offset, cellOffset + 5);
					cell.setCellValue(recipe.getIngredients().get(ingName));
					CellReference cellItemIn = new CellReference(cell);

					cell = sheet.getCell(offset, cellOffset + 1);
					cell.setCellFormula(getNeededItemAmount(cacheCraftAmount.get(name), cellItemIn));
					CellReference itemAmount = new CellReference(cell);

					cell = sheet.getCell(offset, cellOffset + 2);
					cell.setCellFormula(getItemAmount(itemAmount));

					cell = sheet.getCell(offset, cellOffset + 3);
					cell.setCellFormula(getStackAmount(itemAmount));

					cell = sheet.getCell(offset, cellOffset + 4);
					cell.setCellFormula(getDKAmount(itemAmount));

					addToIngrediantCache(cacheIngredientsAmount, ingName, itemAmount);
					addToIngredientsPriceCache(cacheIngredientsList, name, ingName, cellIngName, cellItemIn);
				}
			}
			offset++;
		}
		return offset;
	}

	private static void addToIngrediantCache(HashMap<String, ArrayList<CellReference>> cacheIngredientsAmount, String name, CellReference cellAmount)
	{
		name = toReadable(name);
		if(cacheIngredientsAmount.containsKey(name))
		{
			cacheIngredientsAmount.get(name).add(cellAmount);
		}
		else
		{
			ArrayList<CellReference> amounts = new ArrayList<>();
			amounts.add(cellAmount);
			cacheIngredientsAmount.put(name, amounts);
		}
	}

	private static void addToIngredientsPriceCache(HashMap<String, HashMap<Pair<String, CellReference>, CellReference>> cacheIngredientsList, String name, String ingName,
			CellReference ingNameCell, CellReference inAmount)
	{
		name = toReadable(name);
		if(cacheIngredientsList.containsKey(name))
		{
			cacheIngredientsList.get(name).put(Pair.of(ingName, ingNameCell), inAmount);
		}
		else
		{
			HashMap<Pair<String, CellReference>, CellReference> refs = new HashMap<>();
			refs.put(Pair.of(ingName, ingNameCell), inAmount);
			cacheIngredientsList.put(name, refs);
		}
	}

	private static String getNeededItemAmount(CellReference craftAmount, CellReference itemIn)
	{
		return craftAmount.formatAsString() + "*" + itemIn.formatAsString();
	}

	private static void generateIngredientsSum(HashMap<String, Cell> cacheOverviewIngredientsAmount, HashMap<String, ArrayList<CellReference>> cacheIngredientsAmount)
	{
		for(Entry<String, Cell> ing : cacheOverviewIngredientsAmount.entrySet())
		{
			ing.getValue().setCellFormula(getItemBigSum(cacheIngredientsAmount, ing.getKey()));
		}
	}

	private static String getItemBigSum(HashMap<String, ArrayList<CellReference>> cellCache, String name)
	{
		MinecraftResolver resolvedName = MinecraftResolver.getByReadable(name);
		ArrayList<MinecraftResolver> resolverList = MinecraftResolver.getByTarget(resolvedName == null ? name : resolvedName.getKey());
		if(resolverList.isEmpty())
		{
			return getItemSum(cellCache.get(name));
		}
		StringBuilder builder = new StringBuilder("SUM(");
		builder.append(getItemSum(cellCache.get(name))).append(",");

		if(!resolverList.isEmpty())
		{
			for(MinecraftResolver resolver : resolverList)
			{
				builder.append("ROUNDUP(").append(getItemBigSum(cellCache, toReadable(resolver.getKey())));
				builder.append("/").append(resolver.getTargetAmount()).append(",0)").append("*").append(resolver.getKeyAmount()).append(",");
			}
			builder.deleteCharAt(builder.length() - 1);
		}
		else
		{
			builder.append("0");
		}

		return builder.append(")").toString();
	}

	private static String getItemAmount(CellReference itemAmount)
	{
		return "MOD(" + itemAmount.formatAsString() + ", 64)";
	}

	private static String getStackAmount(CellReference itemAmount)
	{
		return "MOD(ROUNDDOWN(" + itemAmount.formatAsString() + "/64, 0), 54)";
	}

	private static String getDKAmount(CellReference itemAmount)
	{
		return "ROUNDDOWN(" + itemAmount.formatAsString() + "/64/54, 0)";
	}

	private static String toReadable(String input)
	{
		System.out.println(input);
		MinecraftResolver res = MinecraftResolver.getByKey(input);
		if(res == null)
		{
			System.out.println(Calculator.getRecipes().keySet());
			return Calculator.getRecipes().get(input).getName();
		}
		else
		{
			return res.getToReadable();
		}
	}
}
