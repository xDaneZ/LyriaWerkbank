package me.wolf2323.lyriawerkbank;

import me.wolf2323.lyriawerkbank.proxy.ClientProxy;
import me.wolf2323.lyriawerkbank.proxy.CommonProxy;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;


@Mod(LyriaWerkbank.MODID)
public class LyriaWerkbank
{
	public static final String	MODID	= "lyriawerkbank";
	private final CommonProxy	proxy	= DistExecutor.runForDist(() -> ClientProxy::new, () -> CommonProxy::new);

	public LyriaWerkbank()
	{
		FMLJavaModLoadingContext.get().getModEventBus().register(this);

		proxy.construct();
	}

	@SubscribeEvent
	public void setup(FMLCommonSetupEvent event)
	{
		proxy.setup();
	}

	@SubscribeEvent
	public void ready(FMLCommonSetupEvent event)
	{
		proxy.ready();
	}
}
