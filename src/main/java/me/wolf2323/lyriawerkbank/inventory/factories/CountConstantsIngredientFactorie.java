package me.wolf2323.lyriawerkbank.inventory.factories;

import java.util.ArrayList;

import com.google.gson.JsonObject;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient.IItemList;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IIngredientSerializer;


public class CountConstantsIngredientFactorie implements IIngredientSerializer<IngredientOverride>
{
	@Override
	public IngredientOverride parse(PacketBuffer buffer)
	{
		return null;
	}

	@Override
	public IngredientOverride parse(JsonObject json)
	{
		IItemList list = CraftingHelper.getConstant(new ResourceLocation(json.get("constant").getAsString()));

		ItemStack item = new ArrayList<ItemStack>(list.getStacks()).get(0);
		item.setCount(json.get("count").getAsInt());

		return new IngredientOverride(item);
	}

	@Override
	public void write(PacketBuffer buffer, IngredientOverride ingredient)
	{

	}

}
