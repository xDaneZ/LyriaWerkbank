package me.wolf2323.lyriawerkbank.inventory.factories;

import java.util.Arrays;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;


public class IngredientOverride extends Ingredient
{

	public IngredientOverride(ItemStack stack)
	{
		super(Arrays.stream(new Ingredient.SingleItemList[]
		{
				new Ingredient.SingleItemList(stack)
		}));
	}
}
