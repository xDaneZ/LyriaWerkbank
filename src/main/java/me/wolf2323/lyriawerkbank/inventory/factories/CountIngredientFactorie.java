package me.wolf2323.lyriawerkbank.inventory.factories;

import com.google.gson.JsonObject;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IIngredientSerializer;


public class CountIngredientFactorie implements IIngredientSerializer<IngredientOverride>
{
	@Override
	public IngredientOverride parse(PacketBuffer buffer)
	{
		return null;
	}

	@Override
	public IngredientOverride parse(JsonObject json)
	{
		ItemStack itemStack = CraftingHelper.getItemStack(json, true);
		return new IngredientOverride(itemStack);
	}

	@Override
	public void write(PacketBuffer buffer, IngredientOverride ingredient)
	{
	}

}
