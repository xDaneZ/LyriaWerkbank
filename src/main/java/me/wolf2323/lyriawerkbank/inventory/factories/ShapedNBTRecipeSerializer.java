package me.wolf2323.lyriawerkbank.inventory.factories;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mojang.realmsclient.util.JsonUtils;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient.IItemList;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;


public class ShapedNBTRecipeSerializer extends ShapedRecipe.Serializer
{
	private static final Gson				GSON	= new Gson();
	private static final ResourceLocation	NAME	= new ResourceLocation("lyriawerkbank", "crafting_nbt_shaped");

	@Override
	public ShapedRecipe read(ResourceLocation recipeId, JsonObject json)
	{
		JsonObject copy = GSON.fromJson(GSON.toJson(json), JsonObject.class);
		copy.remove("result");
		copy.add("result", GSON.fromJson("{\"item\": \"minecraft:air\"}", JsonObject.class));

		ShapedRecipe recipe = super.read(recipeId, copy);

		JsonObject result = json.get("result").getAsJsonObject();
		IItemList list = CraftingHelper.getConstant(new ResourceLocation(result.get("constant").getAsString()));
		ItemStack item = new ArrayList<ItemStack>(list.getStacks()).get(0);
		item.setCount(JsonUtils.getIntOr("count", result, 1));

		return new ShapedRecipe(recipeId, recipe.getGroup(), recipe.getRecipeWidth(), recipe.getRecipeHeight(), recipe.getIngredients(), item);
	}

	@Override
	public ResourceLocation getName()
	{
		return NAME;
	}
}
