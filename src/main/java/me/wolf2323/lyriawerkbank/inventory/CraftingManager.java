package me.wolf2323.lyriawerkbank.inventory;

import java.util.ArrayList;
import java.util.Optional;

import com.mojang.realmsclient.util.Pair;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.ContainerWorkbench;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;


public class CraftingManager
{
	private static CraftingManager	instance;

	private ContainerWorkbench		container;

	private int						dropAmount;

	public static void setContainer(ContainerWorkbench container)
	{
		if(container == null)
		{
			instance = null;
		}
		else
		{
			instance = new CraftingManager(container);
		}
	}

	private CraftingManager(ContainerWorkbench container)
	{
		this.container = container;
	}

	public static CraftingManager getCraftingManager()
	{
		return instance;
	}

	public boolean craft(IRecipe recipe, boolean shift)
	{
		clearCraftingTable();

		NonNullList<Ingredient> ingredients = recipe.getIngredients();
		ArrayList<ItemStack> ingredientsStacks = getIngredientsAmount(ingredients);
		ArrayList<ItemStack> inventoryStacks = getInventoryItemAmount(ingredientsStacks);

		int amount = canCraft(ingredients, ingredientsStacks, inventoryStacks);
		if(amount == 0)
		{
			return false;
		}
		if(!shift)
		{
			amount = 1;
		}

		moveItems(ingredients, amount);
		return true;
	}

	protected void autoCraft()
	{
		slotClick(0, 0, ClickType.QUICK_MOVE);
	}

	public void autoCraftAndDrop(boolean shift)
	{
		if(dropAmount == 0)
		{
			slotClick(-999, 0, ClickType.PICKUP);
			dropAmount = shift ? 64 : 1;
		}
	}

	private ArrayList<ItemStack> getIngredientsAmount(final NonNullList<Ingredient> ingredients)
	{
		ArrayList<ItemStack> items = new ArrayList<>();
		for(Ingredient i : ingredients)
		{
			if(!i.equals(Ingredient.EMPTY))
			{
				ItemStack stack = i.getMatchingStacks()[0];
				Optional<ItemStack> foundItemStack = items.stream().filter((iStack) -> matchItemstacks(iStack, stack)).findAny();
				if(foundItemStack.isPresent())
				{
					foundItemStack.get().grow(stack.getCount());
				}
				else
				{
					items.add(stack.copy());
				}
			}
		}
		return items;
	}

	private ArrayList<ItemStack> getInventoryItemAmount(final ArrayList<ItemStack> ingredients)
	{
		ArrayList<ItemStack> inventoryStacks = new ArrayList<>();
		for(int i = 10; i < 46; i++)
		{
			ItemStack stack = container.inventorySlots.get(i).getStack();
			Optional<ItemStack> foundItemStack = ingredients.stream().filter((iStack) -> matchItemstacks(iStack, stack)).findAny();
			if(foundItemStack.isPresent())
			{
				Optional<ItemStack> foundItemStackInv = inventoryStacks.stream().filter((iStack) -> matchItemstacks(iStack, stack)).findAny();
				if(foundItemStackInv.isPresent())
				{
					foundItemStackInv.get().grow(stack.getCount());
				}
				else
				{
					inventoryStacks.add(stack.copy());
				}
			}
		}
		return inventoryStacks;
	}

	private void moveItems(NonNullList<Ingredient> ingredients, int amount)
	{
		for(int i = 0; i < 9; i++)
		{
			Ingredient ingredient = ingredients.get(i);
			if(!ingredient.equals(Ingredient.EMPTY))
			{
				ItemStack item = ingredient.getMatchingStacks()[0];
				int toPlace = item.getCount() * amount;

				int placed = 0;
				while(placed < toPlace)
				{
					Pair<Integer, ItemStack> invStack = getFirstSlotWithItem(item);
					int stackCount = invStack.second().getCount();
					slotClick(invStack.first(), 0, ClickType.PICKUP);
					if(toPlace - placed < stackCount)
					{
						slotClickPutBack(toPlace - placed, i + 1);
						slotClick(invStack.first(), 0, ClickType.PICKUP);
					}
					else
					{
						slotClick(i + 1, 0, ClickType.PICKUP);
					}
					placed += stackCount;
				}
			}
		}
	}

	public int canCraft(NonNullList<Ingredient> ingredients, ArrayList<ItemStack> ingredientsStacks, ArrayList<ItemStack> inventoryStacks)
	{
		int amount = 64;
		for(ItemStack stack : ingredientsStacks)
		{
			Optional<ItemStack> foundItemStack = inventoryStacks.stream().filter((iStack) -> matchItemstacks(iStack, stack)).findAny();
			if(!foundItemStack.isPresent())
			{
				return 0;
			}
			int should = stack.getCount();
			int have = foundItemStack.get().getCount();
			if(should > have)
			{
				return 0;
			}
			int max = have / should;
			if(max < amount)
			{
				amount = max;
			}
		}

		for(Ingredient ingredient : ingredients)
		{
			if(!ingredient.equals(Ingredient.EMPTY))
			{
				ItemStack stack = ingredient.getMatchingStacks()[0];
				int stackCount = stack.getCount();
				int possibleAmount = stack.getMaxStackSize() / stackCount;
				if(possibleAmount < amount)
				{
					amount = possibleAmount;
				}
			}
		}

		return amount;
	}

	private void clearCraftingTable()
	{
		slotClick(-999, 0, ClickType.PICKUP);
		for(int i = 1; i < 10; i++)
		{
			ItemStack stack = container.inventorySlots.get(i).getStack().copy();
			if(!stack.isEmpty())
			{
				int amount = stack.getCount();
				slotClick(i, 0, ClickType.PICKUP);
				amount = clearCraftingTablePutInFirstMatching(amount, stack);
				amount = clearCraftingTablePutInFirstEmpty(amount, stack);
				if(amount > 0)
				{
					slotClick(-999, 0, ClickType.PICKUP_ALL);
				}
			}
		}
	}

	private int clearCraftingTablePutInFirstMatching(int amount, ItemStack stack)
	{
		for(int j = 10; j < 46 && amount > 0; j++)
		{
			ItemStack stackJ = container.inventorySlots.get(j).getStack();
			if(stack.isItemEqual(stackJ) && ItemStack.areItemStackTagsEqual(stack, stackJ) && stackJ.getCount() < stackJ.getMaxStackSize())
			{
				amount -= (stackJ.getMaxStackSize() - stackJ.getCount());
				slotClick(j, 0, ClickType.PICKUP);
			}
		}
		return amount;
	}

	private int clearCraftingTablePutInFirstEmpty(int amount, ItemStack stack)
	{
		for(int j = 10; j < 46 && amount > 0; j++)
		{
			ItemStack stackJ = container.inventorySlots.get(j).getStack();
			if(stackJ.isEmpty())
			{
				amount -= (stack.getMaxStackSize());
				slotClick(j, 0, ClickType.PICKUP);
			}
		}
		return amount;
	}

	protected static boolean matchItemstacks(ItemStack a, ItemStack b)
	{
		return a.isItemEqual(b) && a.hasDisplayName() == b.hasDisplayName() && (!a.hasDisplayName() || a.getDisplayName().equals(b.getDisplayName()));
	}

	private Pair<Integer, ItemStack> getFirstSlotWithItem(final ItemStack itemStack)
	{
		for(int i = 10; i < 46; i++)
		{
			ItemStack stack = container.inventorySlots.get(i).getStack().copy();
			if(matchItemstacks(stack, itemStack))
			{
				return Pair.of(i, stack);
			}
		}
		return null;
	}

	private void slotClickPutBack(int amount, int slotId)
	{
		for(int i = 0; i < amount; i++)
		{
			slotClick(slotId, 1, ClickType.PICKUP);
		}
	}

	private void slotClick(int slotId, int dragData, ClickType clickType)
	{
		PlayerControllerMP controller = Minecraft.getInstance().playerController;
		controller.windowClick(container.windowId, slotId, dragData, clickType, Minecraft.getInstance().player);
	}

	public void tick()
	{
		if(dropAmount > 0)
		{
			if(Minecraft.getInstance().player.inventory.getItemStack().isEmpty())
			{
				slotClick(0, 0, ClickType.PICKUP);
			}
			else
			{
				slotClick(-999, 0, ClickType.PICKUP);
				dropAmount--;
			}
		}
	}

	public void breakAutoCraftAndDrop()
	{
		if(dropAmount > 0)
		{
			dropAmount = 0;
			slotClick(-999, 0, ClickType.PICKUP);
		}
	}
}
