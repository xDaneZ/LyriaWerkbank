package me.wolf2323.lyriawerkbank.inventory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import cpw.mods.modlauncher.api.INameMappingService;
import me.wolf2323.lyriawerkbank.util.Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiCrafting;
import net.minecraft.client.gui.recipebook.RecipeList;
import net.minecraft.client.util.RecipeBookClient;
import net.minecraft.client.util.SearchTree;
import net.minecraft.client.util.SearchTreeManager;
import net.minecraft.inventory.ContainerWorkbench;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.RecipeManager;
import net.minecraft.item.crafting.RecipeSerializers;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RecipesUpdatedEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.gameevent.TickEvent;


public class GuiListener
{
	private static final Logger	LOGGER				= LogManager.getLogger();
	public static final int		PATH_PREFIX_LENGTH	= "recipes/".length();
	public static final int		PATH_SUFFIX_LENGTH	= ".json".length();
	private boolean				setup;

	@SubscribeEvent
	public void entityJoinWorldEvent(EntityJoinWorldEvent event)
	{
		if(!setup && event.getEntity() instanceof EntityPlayerSP)
		{
			printDevMessage();
			setup = true;
		}
	}

	public void printDevMessage()
	{
		Minecraft.getInstance().player.sendMessage(new TextComponentString(TextFormatting.DARK_GREEN + "Du benutzt " + TextFormatting.BLUE + "LyriaWerkbank"
				+ TextFormatting.DARK_GREEN + " entwickelt von " + TextFormatting.BLUE + "Wolf2323" + TextFormatting.BLUE + "!"));
	}

	public List<IRecipe> loadLyriaRecipes()
	{
		IResourceManager resourceManager = Minecraft.getInstance().getResourceManager();
		Gson gson = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
		List<IRecipe> recipes = new ArrayList<>();

		for(ResourceLocation resourcelocation : resourceManager.getAllResourceLocations("recipes", (p_199516_0_) ->
		{
			return p_199516_0_.endsWith(".json") && !p_199516_0_.startsWith("_");
		}))
		{
			System.out.println(resourcelocation);
			if(!resourcelocation.getNamespace().equals("lyriawerkbank"))
			{
				continue;
			}

			String s = resourcelocation.getPath();
			ResourceLocation resourcelocation1 = new ResourceLocation(resourcelocation.getNamespace(), s.substring(PATH_PREFIX_LENGTH, s.length() - PATH_SUFFIX_LENGTH));

			try(IResource iresource = resourceManager.getResource(resourcelocation))
			{
				JsonObject jsonobject = JsonUtils.fromJson(gson, IOUtils.toString(iresource.getInputStream(), StandardCharsets.UTF_8), JsonObject.class);
				if(jsonobject == null)
				{
					LOGGER.error("Couldn't load recipe {} as it's null or empty", (Object) resourcelocation1);
				}
				else if(jsonobject.has("conditions") && !net.minecraftforge.common.crafting.CraftingHelper.processConditions(JsonUtils.getJsonArray(jsonobject,
						"conditions")))
				{
					LOGGER.info("Skipping loading recipe {} as it's conditions were not met", resourcelocation1);
				}
				else
				{
					recipes.add(RecipeSerializers.deserialize(resourcelocation1, jsonobject));
				}
			}
			catch(IllegalArgumentException | JsonParseException jsonparseexception)
			{
				LOGGER.error("Parsing error loading recipe {}", resourcelocation1, jsonparseexception);
			}
			catch(IOException ioexception)
			{
				LOGGER.error("Couldn't read custom advancement {} from {}", resourcelocation1, resourcelocation, ioexception);
			}
		}
		return recipes;
	}

	@SubscribeEvent
	public void recipesUpdatedEvent(RecipesUpdatedEvent event)
	{
		RecipeManager manager = Minecraft.getInstance().getConnection().getRecipeManager();
		List<IRecipe> recipes = new ArrayList<>(manager.getRecipes());
		recipes.removeIf((recipe) -> recipe.getId().getNamespace().equals("lyriacraft"));
		manager.clear();

		recipes.addAll(loadLyriaRecipes());

		recipes.forEach(manager::addRecipe);

		SearchTree<RecipeList> searchtree = (SearchTree<RecipeList>) Minecraft.getInstance().<RecipeList> getSearchTree(SearchTreeManager.RECIPES);
		searchtree.clear();
		RecipeBookClient recipebookclient = Minecraft.getInstance().player.getRecipeBook();
		recipebookclient.rebuildTable();
		recipebookclient.getRecipes().forEach(searchtree::add);
		searchtree.recalculate();

		Minecraft.getInstance().getConnection().getRecipeManager().getRecipes().forEach((r) ->
		{
			Minecraft.getInstance().player.getRecipeBook().unlock(r);
		});
	}

	@SubscribeEvent
	public void worldEventLoad(WorldEvent.Load event)
	{
		if(setup && Minecraft.getInstance().player == null)
		{
			setup = false;
		}
	}

	@SubscribeEvent
	public void onGuiOpenEvent(GuiOpenEvent event)
	{
		if(event.getGui() instanceof GuiContainer)
		{
			GuiContainer container = (GuiContainer) event.getGui();
			if(container.inventorySlots instanceof ContainerWorkbench)
			{
				ContainerWorkbench containerWorkbench = (ContainerWorkbench) container.inventorySlots;
				CraftingManager.setContainer(containerWorkbench);
				try
				{
					Utils.reflectSetFinal(GuiCrafting.class.getDeclaredField(ObfuscationReflectionHelper.remapName(INameMappingService.Domain.FIELD, "recipeBookGui")),
							event.getGui(), new GuiRecipeBookOverride());
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				return;
			}
		}
		if(CraftingManager.getCraftingManager() != null)
		{
			CraftingManager.setContainer(null);
		}
	}

	@SubscribeEvent
	public void onTick(TickEvent.ClientTickEvent e)
	{
		if(CraftingManager.getCraftingManager() != null)
		{
			CraftingManager.getCraftingManager().tick();
		}
	}
}
