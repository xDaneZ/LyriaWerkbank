package me.wolf2323.lyriawerkbank.inventory;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.recipebook.GhostRecipe;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;


public class GhostRecipeOverride extends GhostRecipe
{
	@Override
	public void render(Minecraft mc, int x, int y, boolean isTable, float partialTicks)
	{
		super.render(mc, x, y, isTable, partialTicks);
		RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.disableLighting();

		for(int i = 1; i < this.size(); ++i)
		{
			GhostRecipe.GhostIngredient ghostingredient = this.get(i);
			int j = ghostingredient.getX() + x;
			int k = ghostingredient.getY() + y;

			GlStateManager.disableLighting();
			ItemStack itemstack = ghostingredient.getItem();
			ItemRenderer itemRenderer = mc.getItemRenderer();
			itemRenderer.renderItemOverlays(mc.fontRenderer, itemstack, j, k);

			GlStateManager.enableLighting();
		}

		RenderHelper.disableStandardItemLighting();
	}
}
