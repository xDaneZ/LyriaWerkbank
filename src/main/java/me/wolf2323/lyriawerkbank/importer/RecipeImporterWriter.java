package me.wolf2323.lyriawerkbank.importer;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;


public class RecipeImporterWriter
{

	protected static void toFile(RecipeImporterItem[][] datas, RecipeImporterItem dataOut, String trade) throws JsonIOException, IOException
	{
		File fileRoot = new File("").getCanonicalFile().getParentFile();
		File file = new File(fileRoot, "src/main/resources/data/lyriawerkbank/recipes");
		File fileConstants = new File(file, "_constants.json");

		HashMap<Character, RecipeImporterItem> items = getKeysAndAddToConstants(datas, fileConstants);
		JsonObject obj = generateRecipe(items, datas, dataOut);

		File fileRecipe = new File(file, normalizeName(trade));
		fileRecipe.mkdirs();
		File fileRecipeFile = new File(fileRecipe, normalizeName(dataOut.getName()) + ".json");
		fileRecipeFile.delete();
		try(Writer writer = new FileWriter(fileRecipeFile))
		{
			RecipeImporter.GSON.toJson(obj, writer);
		}

		saveToConstants(fileConstants, dataOut, true);
	}

	private static HashMap<Character, RecipeImporterItem> getKeysAndAddToConstants(RecipeImporterItem[][] datas, File fileConstants)
	{
		HashMap<Character, RecipeImporterItem> items = new HashMap<>();
		for(int x = 0; x < 3; x++)
		{
			for(int y = 0; y < 3; y++)
			{
				RecipeImporterItem data = datas[x][y];
				if(!data.getName().startsWith("minecraft:"))
				{
					data.setDataConstant();
					saveToConstants(fileConstants, data, false);
				}
				if(!containsValue(items, data))
				{
					Character key = data.getUnusedKey(items);
					items.put(key, data);
				}
			}
		}
		return items;
	}

	private static JsonObject generateRecipe(HashMap<Character, RecipeImporterItem> items, RecipeImporterItem[][] datas, RecipeImporterItem dataOut)
	{
		JsonObject obj = new JsonObject();
		obj.addProperty("type", "lyriawerkbank:crafting_nbt_shaped");
		obj.addProperty("group", "lyriawerkbank:" + normalizeName(dataOut.getName()));
		obj.addProperty("name", dataOut.getName());

		JsonArray array = new JsonArray();
		obj.add("pattern", array);
		array.add(findKey(items, datas[0][0]) + findKey(items, datas[0][1]) + findKey(items, datas[0][2]));
		array.add(findKey(items, datas[1][0]) + findKey(items, datas[1][1]) + findKey(items, datas[1][2]));
		array.add(findKey(items, datas[2][0]) + findKey(items, datas[2][1]) + findKey(items, datas[2][2]));

		JsonObject keys = new JsonObject();
		obj.add("key", keys);
		items.remove(' ');
		for(Entry<Character, RecipeImporterItem> i : items.entrySet())
		{
			keys.add(i.getKey().toString(), i.getValue().toJsonRecipeIngredient());
		}

		obj.add("result", dataOut.toJsonRecipeResult());

		return obj;
	}

	private static void saveToConstants(File fileConstants, RecipeImporterItem dataOut, boolean override)
	{
		JsonArray array;
		try(Reader reader = new FileReader(fileConstants))
		{
			array = RecipeImporter.GSON.fromJson(reader, JsonArray.class);
		}
		catch(IOException e)
		{
			return;
		}

		overrideJsonConstant(dataOut, array, override);

		try(Writer writer = new FileWriter(fileConstants))
		{
			RecipeImporter.GSON.toJson(array, writer);
		}
		catch(IOException e)
		{
		}
	}

	private static void overrideJsonConstant(RecipeImporterItem dataOut, JsonArray constants, boolean override)
	{
		int i = 0;
		boolean found = false;
		while(i < constants.size())
		{
			String nameConst = constants.get(i).getAsJsonObject().get("name").getAsString();

			if(RecipeImporterWriter.normalizeName(dataOut.getName()).equals(nameConst))
			{
				if(found || override)
				{
					constants.remove(i);
					i--;
				}
				found = true;
			}
			i++;

		}

		if(found && !override)
		{
			return;
		}

		constants.add(dataOut.toJsonConstant(override));
	}

	private static String findKey(HashMap<Character, RecipeImporterItem> items, RecipeImporterItem RecipeImporterItem)
	{
		for(Entry<Character, RecipeImporterItem> i : items.entrySet())
		{
			if(i.getValue().equals(RecipeImporterItem))
			{
				return i.getKey().toString();
			}
		}
		return " ";
	}

	private static boolean containsValue(HashMap<Character, RecipeImporterItem> items, RecipeImporterItem data)
	{
		for(RecipeImporterItem i : items.values())
		{
			if(i.equals(data))
			{
				return true;
			}
		}
		return false;
	}

	protected static String normalizeName(String name)
	{
		return name.toLowerCase().replace(' ', '_').replace("ä", "ae").replace("ü", "ue").replace("ö", "oe").replace("ß", "ss").replace(".", "");
	}
}
