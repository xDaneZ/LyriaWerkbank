package me.wolf2323.lyriawerkbank.importer;

import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;


@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class RecipeImporter
{
	protected static final Gson	GSON	= new GsonBuilder().setLenient().create();

	private Runnable			importTask;
	private int					importTaskDelay;

	@SubscribeEvent
	public void GuiOpenEvent(GuiOpenEvent event)
	{
		ChestContainer chest = isRecipeGUI(event.getGui());
		if(chest == null)
		{
			return;
		}

		importTask = () ->
		{
			try
			{
				printMessage("Saved Recipe " + saveRecipe(chest));
			}
			catch(JsonIOException | IOException e)
			{
				printMessage(TextFormatting.DARK_PURPLE + "Konnte Lyria Rezept nicht Speichern");
			}
		};
	}

	@SubscribeEvent
	public void onClientTick(TickEvent event)
	{
		if(importTask != null)
		{
			importTaskDelay++;
			if(importTaskDelay == 20 * 2)
			{
				importTask.run();
				importTask = null;
				importTaskDelay = 0;
			}
		}
	}

	private ChestContainer isRecipeGUI(Screen gui)
	{
		if(!gui.getTitle().getString().equals("Rezept-Vorschau"))
		{
			return null;
		}

		if(!(gui instanceof ContainerScreen))
		{
			return null;
		}
		Container container = ((ContainerScreen<?>) gui).getContainer();
		if(!(container instanceof ChestContainer))
		{
			return null;
		}

		return (ChestContainer) container;
	}

	private String saveRecipe(ChestContainer chest) throws JsonIOException, IOException
	{
		RecipeImporterItem dataOut = new RecipeImporterItem(chest, 40);
		String trade = dataOut.setDataAdditional(chest.inventorySlots.get(4).getStack());

		RecipeImporterWriter.toFile(getIngredients(chest), dataOut, trade);
		return dataOut.getName();
	}

	private RecipeImporterItem[][] getIngredients(ChestContainer chest)
	{
		RecipeImporterItem[][] datas = new RecipeImporterItem[3][3];
		datas[0][0] = new RecipeImporterItem(chest, 10);
		datas[0][1] = new RecipeImporterItem(chest, 11);
		datas[0][2] = new RecipeImporterItem(chest, 12);
		datas[1][0] = new RecipeImporterItem(chest, 19);
		datas[1][1] = new RecipeImporterItem(chest, 20);
		datas[1][2] = new RecipeImporterItem(chest, 21);
		datas[2][0] = new RecipeImporterItem(chest, 28);
		datas[2][1] = new RecipeImporterItem(chest, 29);
		datas[2][2] = new RecipeImporterItem(chest, 30);
		return datas;
	}

	private void printMessage(String msg)
	{
		Minecraft.getInstance().player.sendMessage(new StringTextComponent(TextFormatting.DARK_GRAY + msg));
	}
}
